﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGun : MonoBehaviour {

    public GameObject Bullet;
    public GameObject CustomParentObject;
    [Range(0.0f, 0.9f)]
    public float deviationStrength = 0.159f;
    [Range(0, 200)]
    public float FireSpeed = 10;
    [Range(0.01f, 10.0f)]
    public float Firerate = 0.3f;       // Time in seconds between each shot.
    public Vector3 BulletStartRotation = new Vector3(0,0,0);
    public bool IsFireing = false;

    Vector3 deviationVector;
    Transform container;
    Transform barrelTip;
    HTC.UnityPlugin.Vive.StickyGrabbable grabbable;
    float cooldown = 0;

	// Use this for initialization
	void Start () {
        if (!CustomParentObject) {
            container = transform.parent.Find("BulletContainer");
        }
        else {
            container = CustomParentObject.transform;
        }
        barrelTip = transform.Find("BarrelTip");
        grabbable = gameObject.GetComponent<HTC.UnityPlugin.Vive.StickyGrabbable>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (cooldown > 0) {
            cooldown -= Time.deltaTime;
        }
        else if (cooldown < 0){
            cooldown = 0;
        }

        if ((grabbable.isGrabbed && Input.GetAxis("HTC_VIU_RightTrigger") > 0.5f || IsFireing) && cooldown <= 0) {

            Vector3 rotation = BulletStartRotation;
            if (BulletStartRotation == new Vector3(0,0,0)) {
                rotation = Random.rotation.eulerAngles;
            }

            cooldown = Firerate;
            GameObject clone = Instantiate(Bullet, barrelTip.position, transform.rotation * Quaternion.Euler(rotation), container);
            deviationVector = new Vector3(0.0f, 0.0f, 0.0f);
            deviationVector += Random.insideUnitSphere.normalized * deviationStrength;
            clone.GetComponent<Rigidbody>().velocity = (barrelTip.forward + deviationVector) * FireSpeed ; // times offset deviationVector
            

            // adding Despawn.cs if it doesn't already have one
            if (clone.GetComponent<Despawn>() == null)
            {
                clone.AddComponent<Despawn>();
            }
        }
	}
}
