﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class DroneBehaviour : MonoBehaviour {
    public float speed = 1f;
    public float rotationSpeed = 2f;
    public float hoverForce = 50f;
    public float hoverHeight = 3f;
    public float patrolFrequency = 15f;
    public float patrolAreaSize = 2.5f;

    private Rigidbody droneRigidbody;
    private float timeSincePatrol = 0f;
    private Vector3 patrolLocation;
    private Vector3 startPosition;

    void Awake() {
        droneRigidbody = GetComponent<Rigidbody>();
        droneRigidbody.drag = 1f;
        droneRigidbody.angularDrag = 0.5f;
        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        patrolLocation.x = startPosition.x + Random.Range(-patrolAreaSize, patrolAreaSize);
        patrolLocation.z = startPosition.z + Random.Range(-patrolAreaSize, patrolAreaSize);
    }

    //  Update patrol location with regular intervals
    void Update() {
        timeSincePatrol += Time.deltaTime;

        if (timeSincePatrol > patrolFrequency) {
            timeSincePatrol -= patrolFrequency;

            patrolLocation.x = startPosition.x + Random.Range(-patrolAreaSize, patrolAreaSize);
            patrolLocation.z = startPosition.z + Random.Range(-patrolAreaSize, patrolAreaSize);
        }
    }

    //  Maintain distance to the ground, and move to patrol location
    void FixedUpdate() {
        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hit;
        float proportionalHeight = 0f;
        float rayHeight = 0f;
        float worldHeight = 0f;

        //  Use raycast to go higher if there's something beneath you, but never go below a given height
        if (Physics.Raycast(ray, out hit, hoverHeight)) {
            rayHeight = (hoverHeight - hit.distance) / hoverHeight;
        }
        worldHeight = (hoverHeight - transform.position.y) / hoverHeight;

        proportionalHeight = Mathf.Max(rayHeight, worldHeight);
        Vector3 appliedHoverForce = transform.up * proportionalHeight * hoverForce;
        droneRigidbody.AddForce(appliedHoverForce, ForceMode.Acceleration);

        if (droneRigidbody.rotation.eulerAngles != Vector3.zero) {
            Vector3 rotationOffset = -transform.up - Vector3.down;
            droneRigidbody.transform.Rotate(rotationOffset * rotationSpeed);
        }

        Vector3 offset = new Vector3(patrolLocation.x - transform.position.x, 0f, patrolLocation.z - transform.position.z) * speed;
        droneRigidbody.AddForce(offset, ForceMode.Acceleration);
    }

    //  Move away from anything inside the sphereCollider. The closer it is, the faster you move away from it
    private void OnTriggerStay(Collider other) {
        Vector3 offset = (transform.position - other.transform.position);
        offset /= offset.magnitude * offset.magnitude;
        droneRigidbody.AddForce(offset * speed, ForceMode.Acceleration);
    }
}
