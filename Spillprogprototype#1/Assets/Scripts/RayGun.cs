﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayGun : MonoBehaviour {

    public float DamagePerSecond = 100;
    public float Range = 100;

    HTC.UnityPlugin.Vive.StickyGrabbable grabbable;
    CapsuleCollider beamTrigger;
    LineRenderer lineRender;
    Transform barrelTip;

    // Use this for initialization
    void Start () {

        beamTrigger = GetComponentInChildren<CapsuleCollider>();
        lineRender = GetComponentInChildren<LineRenderer>();
        grabbable = gameObject.GetComponent<HTC.UnityPlugin.Vive.StickyGrabbable>();
        barrelTip = transform.Find("BarrelTip");

    }

    // Update is called once per frame
    void Update () {

        if ((grabbable.isGrabbed && Input.GetAxis("HTC_VIU_RightTrigger") > 0.5f)) {
            lineRender.enabled = true;
        }
       else {
            lineRender.enabled = false;
        }
    }

    private void FixedUpdate() {

        if ((grabbable.isGrabbed && Input.GetAxis("HTC_VIU_RightTrigger") > 0.5f)) {

            Ray ray = new Ray(barrelTip.position, transform.right);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Range)) {
                Debug.Log("Raycast: " + hit.collider.gameObject.name);
                if (hit.collider.GetComponent<FragileObject>()) {

                    hit.collider.GetComponent<FragileObject>().health -= DamagePerSecond * Time.deltaTime;
                }
            }
        }
    }

}
