﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]     // Select this parent object when any part of the drone is clicked in editor.
public class KinematicDrone : MonoBehaviour {

    public float Speed = 2;
    public float DistanceToPickUpBox = 1.5f;
    public float DistanceToAttachBox = 0.8f;   // The distance from the drone to the box after it is attached. 0 is box center = drone center.
    public float VelocityKnockback = 0.5f;        // How much collisions affect drone's velocity.
    public float RotationKnockback = 70;        // How much collisions affect drone's velocity.
    public float knockbackVelCorrection = 1.2f;       // How quickly the drone gets back to it's original velocity.
    public float knockbackRotCorrection = 2.5f;       // How quickly the drone gets back to it's original rotation.
    public float OnDeathTossSpeed = 20;              // How fast the drone tosses the box at the center of the map when it dies.

    Rigidbody rig;                  // The rigidbody of the drone.
    FixedJoint joint;               // Joint that will be created when picks up a box not defined on start.
    GameManager gameManager;
    Transform targetBox;
    Transform mapCenter;

    Vector3 knockbackVelocity = Vector3.zero;
    Vector3 knockbackRotation = Vector3.zero;
    Vector3 angleVel = Vector3.zero;

    //##############################################https://stackoverflow.com/questions/30290262/how-to-throw-a-ball-to-a-specific-point-on-plane

    // Throws ball at location with regards to gravity (assuming no obstacles in path) and initialVelocity (how hard to throw the ball)
    public Vector3 TrajectoryCalculation(Vector3 targetLocation, float initialVelocity) {
        Vector3 direction = (targetLocation - transform.position).normalized;
        float distance = Vector3.Distance(targetLocation, transform.position);

        float firingElevationAngle = FiringElevationAngle(Physics.gravity.magnitude, distance, initialVelocity);
        Vector3 elevation = Quaternion.AngleAxis(firingElevationAngle, transform.right) * transform.up;
        float directionAngle = AngleBetweenAboutAxis(transform.forward, direction, transform.up);
        Vector3 velocity = Quaternion.AngleAxis(directionAngle, transform.up) * elevation * initialVelocity;

        return velocity;
    }

    // Helper method to find angle between two points (v1 & v2) with respect to axis n
    public static float AngleBetweenAboutAxis(Vector3 v1, Vector3 v2, Vector3 n) {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    // Helper method to find angle of elevation (ballistic trajectory) required to reach distance with initialVelocity
    // Does not take wind resistance into consideration.
    private float FiringElevationAngle(float gravity, float distance, float initialVelocity) {
        float angle = 0.5f * Mathf.Asin((gravity * distance) / (initialVelocity * initialVelocity)) * Mathf.Rad2Deg;
        return angle;
    }
    //##############################################


    // Use this for initialization
    void Start () {
        rig = GetComponent<Rigidbody>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>(); // GameObject.Find is expensive. Must only be done on startup.
        if (gameManager == null) {
            Debug.LogError("There are no GameManager in the scene with the name 'GameManager'");
        }

        mapCenter = GameObject.Find("MapCenter").transform;
        if (mapCenter == null) {
            Debug.LogError("There are no gameObject in the scene with the name 'MapCenter'");
        }

    }

    // Update is called once per frame
    void Update () {

        if (knockbackVelocity.magnitude > 0.01f) {
            knockbackVelocity *= 1 - knockbackVelCorrection * Time.deltaTime;
        }
        else {
            knockbackVelocity = Vector3.zero;
        }

        if (knockbackRotation.magnitude > 0.01f) {
            knockbackRotation *= 1 - knockbackRotCorrection * Time.deltaTime;

        }
        else {
            knockbackRotation = Vector3.zero;
        }

        if (angleVel.magnitude > 0.01f) {
            angleVel *= 1 - knockbackRotCorrection * Time.deltaTime;

        }
        else {
            angleVel = Vector3.zero;
        }


    }

    void move(Vector3 dir) {

        rig.MovePosition(transform.position + ((dir * Speed) + knockbackVelocity)  * Time.deltaTime);
        
    }

    void findNewTarget() {
        float distance = float.MaxValue;
        int boxIndex = 0;

        if (gameManager.availableBoxes.Count > 0) {

            foreach (GameObject box in gameManager.availableBoxes) {
                if (Vector3.Distance(transform.position, box.transform.position) < distance) {
                    distance = Vector3.Distance(transform.position, box.transform.position);
                    boxIndex = gameManager.availableBoxes.IndexOf(box);
                }
            }

            targetBox = gameManager.availableBoxes[boxIndex].transform;
        }
    }

    void FixedUpdate() {
        if (joint == null) {         // Don't have a box yet. Must get to a box.
            
            // If not target box, select the one available that is closest.
            if (targetBox == null) {
                findNewTarget();
            }
            else if (!gameManager.availableBoxes.Contains(targetBox.gameObject)) { // Else if i already have a target, check that it's still available. 
                findNewTarget();
            }

            if (targetBox != null) {

                // Colse enough to pickup my target box? If so move the box and create joint to hold it.
                if (Vector3.Distance(transform.position, targetBox.position) < DistanceToPickUpBox) {

                    gameManager.availableBoxes.Remove(targetBox.gameObject);
                    targetBox.SetPositionAndRotation(transform.position - new Vector3(0, DistanceToAttachBox, 0), transform.rotation);
                    joint = gameObject.AddComponent<FixedJoint>();      // create joint that will hold us in place.
                    joint.connectedBody = targetBox.gameObject.GetComponent<Rigidbody>();
                }

                // Move toward target box:
                move((targetBox.position - transform.position).normalized);

            }
        }
        else {                      //  Have a box now. Time to leave.
            // Move away from map canter. Get to box despawn zone.
            move((transform.position - mapCenter.position).normalized);


            if (joint.connectedBody == null) {      // Had box, but it despawned.
                targetBox = null;
                Destroy(joint);
            }
        }

        knockbackRotation += angleVel * Time.deltaTime;
        rig.MoveRotation(Quaternion.Euler(knockbackRotation));
    }

    private void OnCollisionEnter(Collision collision) {

        if (collision.rigidbody != null) {

            knockbackVelocity = collision.rigidbody.velocity * VelocityKnockback;
            angleVel += new Vector3(Random.value, Random.value, Random.value) * RotationKnockback * collision.rigidbody.velocity.magnitude;
        }
    }

    private void OnDestroy() {

        if (joint != null && joint.connectedBody != null) {
            gameManager.availableBoxes.Add(targetBox.gameObject);     // Add the box back into available list.

            //Rigidbody boxRigid = joint.connectedBody;
            Respawn boxRespawn = joint.connectedBody.GetComponent<Respawn>();
            if (boxRespawn == null) {
                Debug.LogError("The box "+ gameObject.name +" was carrying did not have a respawn script.");
            }
            Destroy(joint);
            boxRespawn.RespawnObject();
            //boxRigid.velocity = (mapCenter.position - joint.gameObject.transform.position).normalized * OnDeathTossSpeed;
            //boxRigid.AddForce(TrajectoryCalculation(mapCenter.position, OnDeathTossSpeed), ForceMode.VelocityChange); // Returns {nan, nan, nan} sometimes.

            Debug.Log("Drone with box destroyed tossing it back with speed: " + OnDeathTossSpeed);
        }
    }

}
