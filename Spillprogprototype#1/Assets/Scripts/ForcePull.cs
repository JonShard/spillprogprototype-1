﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[SelectionBase]
public class ForcePull : MonoBehaviour {

    [HideInInspector]
    public bool isOn = false;

    [System.Serializable]
    public struct Config {
        public float ForceMultiplier;               // The amound of acceleration objects inside beam will have towards stasis bubble.
        public float StasisRadius;                  // Actual radius of beam. Changing this requires you to change particle system radius and center cube sphere collider radius.
        public float StasisForce;                   // What base velocity will objects move towads the center. (At border.)
        public float StasisAngleVelocity;           // What base speed is used for angular velocity of everyting inside stasis.
        public float StasisStopDistance;            // The distance drom the center objects will stop moving at.
    };

    struct References {
        public GameObject model;                                // The actual model of the gun.
        public BoxCollider beamTrigger;                         // The very long rectangular trigger for affecting objects from afar.
        public SphereCollider bubbleTrigger;                    // The Spherical trigger that sits on the gun.
        public SphereCollider centerCollider;                   // The center collider that prevents objects from tangling up in the center of the spheretrigger.
        public ParticleSystem bubbleParticles;                  // Particles to mark the border og the sphere trigger.
        public ParticleSystem beamParticles;                    // Particles to mark the reach of the beam trigger. Makes it easier to aim beam.
        public ParticleSystem.EmissionModule bubbleEmission;    
        public ParticleSystem.EmissionModule beamEmission;
        public Vector3 beamSize;
        public float bubbleRadius;                              // Only used for sphere trigger, nice to keep seperate.
        public float centerColliderRadius;
    };

    public Config Gravity;
    public Config AntiGravity;

    References gravityRefs;
    References antiGravityRefs;

    [HideInInspector]
    public Config ActiveConfig;       // Public struct instance is euqal to one of the two configurations and are read by ForcePullable objects.
    private References activeRefs;

    bool fireingBeam;
    bool fireingSphere;
    bool modeSwitch;

    // Use this for initialization
    void Start () {

        // Setting up all references to child game objects, so we can to things like, swap model, enable / disable particles and colliders:
        // GravityGun:
        Transform gravityPullTrigger = transform.Find("GravityGun").Find("ForcePullTrigger");
        gravityRefs.model =             gravityPullTrigger.parent.Find("Model").gameObject;
        gravityRefs.beamTrigger =       gravityPullTrigger.gameObject.GetComponentInChildren<BoxCollider>();          // Important, this box collider must be the first one under Gun object.
        gravityRefs.bubbleTrigger =     gravityPullTrigger.gameObject.GetComponentInChildren<SphereCollider>();
        gravityRefs.bubbleParticles =   gravityPullTrigger.Find("Center").GetComponentInChildren<ParticleSystem>();
        gravityRefs.beamParticles =     gravityPullTrigger.Find("BeamParticleEffect").GetComponent<ParticleSystem>();
        gravityRefs.centerCollider =    gravityPullTrigger.Find("Center").GetComponentInChildren<SphereCollider>();
        gravityRefs.bubbleEmission =    gravityRefs.bubbleParticles.emission;                                            // Get emisson object to toggle particleeffect.
        gravityRefs.beamEmission =      gravityRefs.beamParticles.emission;
        gravityRefs.beamSize =          gravityRefs.beamTrigger.size;                                                    // Store the size of the beam so it can be set to zero later, and restored after.
        gravityRefs.bubbleRadius =      gravityRefs.bubbleTrigger.radius;
        gravityRefs.bubbleTrigger.radius = 0;
        gravityRefs.centerColliderRadius = 0.13f;
        
        // AntiGravityGun:
        Transform antiGravityPullTrigger =  transform.Find("AntiGravityGun").Find("ForcePullTrigger");
        antiGravityRefs.model =             antiGravityPullTrigger.parent.Find("Model").gameObject;
        antiGravityRefs.beamTrigger =       antiGravityPullTrigger.gameObject.GetComponentInChildren<BoxCollider>();          // Important, this box collider must be the first one under Gun object.
        antiGravityRefs.bubbleTrigger =     antiGravityPullTrigger.gameObject.GetComponentInChildren<SphereCollider>();
        antiGravityRefs.bubbleParticles =   antiGravityPullTrigger.Find("Center").GetComponentInChildren<ParticleSystem>();
        antiGravityRefs.beamParticles =     antiGravityPullTrigger.Find("BeamParticleEffect").GetComponent<ParticleSystem>();
        antiGravityRefs.centerCollider =    antiGravityPullTrigger.Find("Center").GetComponentInChildren<SphereCollider>();
        antiGravityRefs.bubbleEmission =    antiGravityRefs.bubbleParticles.emission;                                            // Get emisson object to toggle particleeffect.
        antiGravityRefs.beamEmission =      antiGravityRefs.beamParticles.emission;
        antiGravityRefs.beamSize =          antiGravityRefs.beamTrigger.size;                                                    // Store the size of the beam so it can be set to zero later, and restored after.
        antiGravityRefs.bubbleRadius =      antiGravityRefs.bubbleTrigger.radius;
        antiGravityRefs.bubbleTrigger.radius = 0;
        antiGravityRefs.centerColliderRadius = 0;



        gravityRefs.beamTrigger.size = Vector3.zero;
        gravityRefs.beamEmission.enabled = false;
        gravityRefs.bubbleTrigger.radius = 0;
        gravityRefs.bubbleEmission.enabled = false;
        gravityRefs.centerCollider.radius = 0;

        antiGravityRefs.beamTrigger.size = Vector3.zero;
        antiGravityRefs.beamEmission.enabled = false;
        antiGravityRefs.bubbleTrigger.radius = 0;
        antiGravityRefs.bubbleEmission.enabled = false;
        antiGravityRefs.model.SetActive(false);

        antiGravityRefs.centerCollider.enabled = false;
        ActiveConfig = Gravity;
        activeRefs = gravityRefs;
        fireingBeam = false;
        fireingSphere = false;
        modeSwitch = false;

    }

    public void ToggleMode() {

        modeSwitch = true;  // Since VivePlugginUtility doesn't support sending 2 parameters, the second one has to be set as a global variable.
        FireBeam(false);
        FireSphere(false);
        modeSwitch = false; // Global vairiable (second parameter must the be unset).

        activeRefs.model.SetActive(false);
        if (ActiveConfig.Equals(Gravity)) {   
            ActiveConfig = AntiGravity;
            activeRefs = antiGravityRefs;
        }
        else {
            ActiveConfig = Gravity;
            activeRefs = gravityRefs;
        }
        activeRefs.model.SetActive(true);

        FireBeam(fireingBeam);
        FireSphere(fireingSphere);
    }

    public void FireBeam(bool state) {
        if (!modeSwitch) fireingBeam = state;
        if (state) {
            isOn = true;
            activeRefs.beamTrigger.size = activeRefs.beamSize;
            activeRefs.beamEmission.enabled = true;
        }
        else {
            activeRefs.beamTrigger.size = Vector3.zero;
            activeRefs.beamEmission.enabled = false;
        }
    }

    public void FireSphere(bool state) {
        if (!modeSwitch) fireingSphere = state;
        if (state) {
            isOn = true;
            activeRefs.bubbleTrigger.radius = activeRefs.bubbleRadius;
            activeRefs.bubbleEmission.enabled = true;
            activeRefs.centerCollider.radius = activeRefs.centerColliderRadius;
        }
        else {
            isOn = false;
            activeRefs.bubbleTrigger.radius = 0;
            activeRefs.bubbleEmission.enabled = false;
            activeRefs.centerCollider.radius = 0;

        }
    }

}
