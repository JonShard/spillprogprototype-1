﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FragileObject : MonoBehaviour {

    public float health = 100;
    public float collisionResistance = 10f;
    public int explosionForce = 20;
    public float knifeDamageMultilier = 8;
    public GameObject explosionPrefab;
    public float pointsForKilling = 100;
    public float timeBonus = 100;           // The amount of points from timebonus you get if killing the enemy the frame it spawns.
    public float timeBonusTime = 10;        // The total time in seconds it takes for the timebonus to go from 100% to 0%. Linear reduction.

    GameManager gameManager;
    [SerializeField]
    float bonusTimeLeft;

    private void Start() {
        bonusTimeLeft = timeBonusTime;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

    }
    void die() {
            Debug.Log("Fragile object: " + gameObject.name + " exploded!!");
        gameManager.IncreaseScore((int)(pointsForKilling + timeBonus * (bonusTimeLeft / timeBonusTime))); // Give player score based on timebonus.
            if (explosionPrefab) {
                explosionPrefab.GetComponent<UnityStandardAssets.Effects.ExplosionPhysicsForce>().explosionForce = this.explosionForce;
                explosionPrefab.GetComponent<UnityStandardAssets.Effects.ParticleSystemMultiplier>().multiplier = (float)this.explosionForce / 20f;
                Instantiate(explosionPrefab, transform.position, transform.rotation);
            }
            Destroy(gameObject);
        
    }
                                        
    private void Update() {
        if (health < 0f) {
            die();
        }

        if (bonusTimeLeft > 0) {
            bonusTimeLeft -= Time.deltaTime;
        }
        else {
            bonusTimeLeft = 0;
        }
    }

    //  lose health on collision, explode if health is zero
    private void OnCollisionEnter(Collision collision) {
        float damage = 0;
        //  Slow collisions do nothing, harder collisions deal more damage
        if (collision.relativeVelocity.magnitude > collisionResistance) {
            damage = collision.relativeVelocity.magnitude - collisionResistance;
        }
        if (collision.gameObject.GetComponent<Knife>() != null) {
            damage *= knifeDamageMultilier;
        }
        health -= damage;
    }
}
