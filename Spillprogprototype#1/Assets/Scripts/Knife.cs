﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour {

    Rigidbody rig;                  // The rigidbody of the knife.
    FixedJoint joint;               // Joint that will be created when knife sticks to something.
    HTC.UnityPlugin.Vive.BasicGrabbable grabbable;
    ForcePullable pullable;
    GameManager gameManager;

    void Start () {

        rig = this.gameObject.GetComponent<Rigidbody>();                    // Do we have a rigidbody? Try self
        if (rig == null) {
            Debug.LogWarning("Missing component, checking parent.");
           rig = this.GetComponentInParent<Rigidbody>();
            if (rig != null) {                                              // Try parent.
                Debug.Log("Using Rigidbody body in parent.");
            }
        }
        if (rig == null) {
            Debug.LogError("Missing component!! No rigidbodyin parent.");   // No rigidbody, throw error.
        }
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>(); // GameObject.Find is expensive. Must only be done on startup.
        if (gameManager == null) {
            Debug.LogError("There are no GameManager in the scene with the name 'GameManager'");
        }

        grabbable = GetComponent<HTC.UnityPlugin.Vive.BasicGrabbable>();
        pullable = GetComponent<ForcePullable>();
    }

    private void FixedUpdate() {
        if (joint != null) {                                            // If can be ditached. 
     
            if (grabbable.isGrabbed) {                                  // Ditach if grabbed.
                Destroy(joint);
            }

            if (pullable.isBeingPulled()) {                             // Ditach if force pulled.
                Destroy(joint);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)                  // When we inter an object.
    {
        if (collision.gameObject.GetComponent<KnifeStick>() != null && rig != null && joint == null) {
            
            gameManager.IncreaseScore(collision.gameObject.GetComponent<KnifeStick>().score);
            if (collision.gameObject.GetComponent<KnifeStick>().RequiredSpeed < rig.velocity.magnitude) {

              
                if (joint == null) {                                    // If we hit something, and we're not already stuck to something,
                    joint = gameObject.AddComponent<FixedJoint>();      // create joint that will hold us in place.
                    joint.connectedBody = collision.gameObject.GetComponent<Rigidbody>();
                }
            }
        }
    }
}
