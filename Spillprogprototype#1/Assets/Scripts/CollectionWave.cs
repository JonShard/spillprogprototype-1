﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionWave : MonoBehaviour {

    /*
    public bool ElevatorMode = false;
    public PhysicalButton ElevatorButton;
    public int ElevatorCountdown = 0;
    */

    // TODO: Implement WaitForFirstBox.
    // public bool WaitForFirstBox = true;      // If enabled, the coutdown starts after the first box is within player area.
    public int CountdownTime = 60;              // The time in minutes the player gets to collect boxes.
    public int StopSpawningEarly = 10;          // Time in seconds before the wave ends in which items will stop spawning from belts.

    float timeLeft;
    bool boxInPlayerArea = false;
    bool postStart = true;

    // Use this for initialization
    void Start () {
        timeLeft = CountdownTime;
	}
	
	// Update is called once per frame
	void Update () {
        if (postStart) {        // If first time we so update() start the Convayourbelts.
            foreach (GameObject belt in GameObject.FindGameObjectsWithTag("ConveyourBelt")) {
                belt.GetComponent<ConveyourBelt>().SetSpawningEnabled(true);
            }
        }

        // Stop spawning boxes some time before wave is destroyed. 
        if (timeLeft - StopSpawningEarly < 0) {
            foreach (GameObject belt in GameObject.FindGameObjectsWithTag("ConveyourBelt")) {
                belt.GetComponent<ConveyourBelt>().SetSpawningEnabled(false);
            }
        }

        if (timeLeft > 0) {
                timeLeft -= Time.deltaTime;
        }
        else {
            Destroy(gameObject);
        }
	}
}
