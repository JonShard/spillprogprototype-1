﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeGun : MonoBehaviour {

    public GameObject Bullet;
    public GameObject CustomParentObject;
    [Range(0, 1)]
    public float Cooldown = 0.3f;           // The amount of seconds between each possible knife spawn.
    GameObject clone;

    Transform container;
    Transform barrelTip;
    HTC.UnityPlugin.Vive.StickyGrabbable grabbable;

    private bool hold;
    float cooldownTime = 0;

    // Use this for initialization
    void Start()
    {
        hold = false;
        if (!CustomParentObject)
        {
            container = transform.parent.Find("BulletContainer");
        }
        else
        {
            container = CustomParentObject.transform;
        }
        barrelTip = transform.Find("BarrelTip");
        grabbable = gameObject.GetComponent<HTC.UnityPlugin.Vive.StickyGrabbable>();

    }

    private void Update() {
        if (cooldownTime > 0) {
            cooldownTime -= Time.deltaTime;
        }
        else {
            cooldownTime = 0;
            //Sound que, recharge done.
        }
    }

    // Update is called once per frame
    public void Fire(bool state)
    {
        if (state) {
            if (!hold && cooldownTime <= 0) {
                clone = Instantiate(Bullet, barrelTip.position, transform.rotation * Quaternion.Euler(new Vector3(90.0f, 90.0f, 0.0f)), container);
                cooldownTime = Cooldown;
                hold = true;       // hold trigger for the first frame
            }   // spawn knife above gun
            // TODO: Create joint.
        }
        else {
            hold = state;     // already holding down trigger
            // TODO: Destroy joint.
        }
    }
}
