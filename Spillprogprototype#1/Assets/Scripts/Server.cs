﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Server : MonoBehaviour {

    private string url = "https://knifethrowingscore.herokuapp.com/scores";
    public Scorelist s;
    Text highscoreLabel;

    public void POST(int score, string username)
    {
        WWW www;
        WWWForm form = new WWWForm();
        form.AddField("score", score);
        form.AddField("username", username);
        www = new WWW(url, form);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW data)
    {
        yield return data;
        GET();
    }


    IEnumerator WaitForGet(WWW data)
    {
        yield return data;
        string structuredHighscores = "";
        if(data.error != null)
        {
            Debug.Log("Error sending request: " + data.error);
        }else
        {
           s = JsonUtility.FromJson<Scorelist>("{\"scores\":" + data.text.ToString() + "}");
            for (int i = 0; i < s.scores.Length; i++)
            {
                structuredHighscores += "user: " + s.scores[i].username.ToString() + " score: " + s.scores[i].score.ToString() + "\n";
            }
        }

        UpdateUI(structuredHighscores);
    }

    public void GET()
    {
        WWW www = new WWW(url);
        StartCoroutine(WaitForGet(www));
    }


    private void UpdateUI(string input)
    {
        GameObject gameOverMenu = GameObject.Find("GameOverMenu");
        highscoreLabel = gameOverMenu.transform.Find("Canvas").transform.Find("lbl_highscores").GetComponent<Text>();
        highscoreLabel.text = input;
    }


    [System.Serializable]
    public class Scorelist
    {

        public Score[] scores;

    }

    [System.Serializable]
    public class Score
    {
        public int score;
        public string username;
    }
}
