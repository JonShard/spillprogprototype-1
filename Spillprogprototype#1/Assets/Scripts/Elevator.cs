﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

    Animation anim;
    GameManager gameManager;


	// Use this for initialization
	void Start () {
        anim = transform.Find("LiftBody").GetComponent<Animation>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
	
    // Lowers the elevator below floorlevel, despawns the boxes, then comes back up. 
    public void CallElevator() {
       // if (!anim.isPlaying /*AND player not standing on platform*/) {
            anim.Play();
       // }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("HealthBox")) {
            gameManager.HarvestBox(other.gameObject);
        }
        else if (other.CompareTag("Junk")) {
            Destroy(other);
        }
    }

}
