﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour {

    int childCount;

	// Use this for initialization
	void Start () {

        childCount = transform.childCount;                          // Store amount of children to monitor when it changes.
        for (int i = 0; i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(false);      // Deactivates all children incase they are not already.
        }

        transform.GetChild(0).gameObject.SetActive(true);           // Start the first wave.
    }
	
	// Update is called once per frame
	void Update () {
		
        if (childCount != transform.childCount) {

            childCount = transform.childCount;
            transform.GetChild(0).gameObject.SetActive(true);           // Start the next wave. Still index 0 since the last wave destroyed itself.
        }
    }
}
