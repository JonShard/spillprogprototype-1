﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Equipment : MonoBehaviour {

    [Range(0,1)]
    public float teleportationBttonSize = 0.60f;

    bool leftHand = false;
    float itemSize = 0;     // The amount of angles a single item makes up in the radial menu. 2 items means 180 degres for each.
    int activeSlot = 0;

    [SerializeField]
    List<GameObject> EquipmentSlots;
    Vector2 padInput;

    GameObject menu;            // The radial mennu for selecting equipment.
    Text label;                 // Temporary label saying what item is equiped.

    // Use this for initialization
    void Start () {
		if (transform.parent.parent.gameObject.name == "Left") {
            leftHand = true;
        }
        Transform slots = transform.Find("Slots");
        for (int i = 0; i < slots.childCount; i++) {
            EquipmentSlots.Add(slots.GetChild(i).gameObject);   // Store all equipment objects in list.
            if (i > 0) {
                slots.GetChild(i).gameObject.SetActive(false);      // Disable all equipment objects except first one.
            }
            else {
                slots.GetChild(i).gameObject.SetActive(true);      // Enable the object in the first equipment slot.
            }
        }

        itemSize = 360 / EquipmentSlots.Count;
        Debug.Log("Equipment count: " + EquipmentSlots.Count);
        Debug.Log("ItemSize " + itemSize);

        menu = transform.Find("Menu").gameObject;
        label = menu.transform.Find("Canvas").Find("lbl_equip").GetComponent<Text>();

    }
	
    private void selectSlot(int slot) {
        Debug.Log("Switching equipment, from " + EquipmentSlots[activeSlot].name + " to " + EquipmentSlots[slot].name + ".");
        EquipmentSlots[activeSlot].SetActive(false);
        EquipmentSlots[slot].SetActive(true);
        activeSlot = slot;

    }



    // Update is called once per frame
    void Update () {

        if (leftHand)   padInput = new Vector2(Input.GetAxis("HTC_VIU_LeftTrackpadHorizontal"), Input.GetAxis("HTC_VIU_LeftTrackpadVertical"));
        else            padInput = new Vector2(Input.GetAxis("HTC_VIU_RightTrackpadHorizontal"), Input.GetAxis("HTC_VIU_RightTrackpadVertical"));

        if (padInput.magnitude > teleportationBttonSize) {

            float angle = Vector2.SignedAngle(Vector2.down, padInput) + 180;
            if (angle != 180) {
                menu.SetActive(true);
                label.text = EquipmentSlots[activeSlot].name;
                selectSlot(Mathf.FloorToInt(angle / itemSize));
            }
        }
        else {
            menu.SetActive(false);
        }


    }
}
