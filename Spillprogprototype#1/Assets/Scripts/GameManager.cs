﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;


public class GameManager : MonoBehaviour {

    public float DistanceToLooseBox = 10;           // The distance a box has to be from map center to die.
    [SerializeField]
    Vector3 BoxRespawnOffset = new Vector3(0,4,0);
    [SerializeField]
    int Score = 0;                           // Score of the player.
    [SerializeField]
    float PlayerBoxOwnershipRadius = 12;

    public List<GameObject> healthBoxes;
    public List<GameObject> availableBoxes;
    public string username;

    Transform mapCenter;
    GameObject vivePointers;
    GameObject gameOverMenu;
    Text scoreLabel;

    Server highscoreServer;
    private bool serverLogged;

    // Use this for initialization
	void Start () {
        username = "Spillprog";
        serverLogged = false;
        Score = 0;

        GameObject[] array = GameObject.FindGameObjectsWithTag("HealthBox");
        foreach (GameObject box in array) {
            healthBoxes.Add(box);
        }
        foreach (GameObject box in array) {
            availableBoxes.Add(box);
        }

        mapCenter = GameObject.Find("MapCenter").transform;
        if (mapCenter == null) {
            Debug.LogError("There are no gameObject in the scene with the name 'MapCenter'");
        }

        vivePointers = GameObject.Find("VivePointers");
        if (vivePointers == null) {
            Debug.LogError("There are no VivePointers in the scene with the name 'VivePointers'");
        }

        gameOverMenu = GameObject.Find("GameOverMenu");
        if (gameOverMenu == null) {
            Debug.LogError("There are no GameOverMenu in the scene with the name 'GameOverMenu'");
        }

        scoreLabel = gameOverMenu.transform.Find("Canvas").transform.Find("lbl_scorevalue").GetComponent<Text>();
        if (scoreLabel == null) {
            Debug.LogError("There are no GameOverMenu in the scene with a Text object with the name 'lbl_scorevalue'");
        }
        disableGameOverMenu();

    }


    public void IncreaseScore(int points) {
        Debug.Log("Score increase: " + points.ToString());
        Score += Mathf.Abs(points);
    }
    public int GetScore() {
        return Score;
    }

    public void RestartLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    public void HarvestBox(GameObject box) {
        IncreaseScore(100);
        healthBoxes.Remove(box);
        availableBoxes.Remove(box);
        Destroy(box);
    }

    private void setWavesActive(bool state) {       // loops trough all waveControllers in the scene, and enables / disables them.

        GameObject[] waveControllers = GameObject.FindGameObjectsWithTag("WaveController");
        foreach (GameObject controller in waveControllers) {
            controller.SetActive(state);
        }
    }


    private void logScore(int playerScore) {
        // TODO: Log player score.
        // if(gameObject.AddComponent<Server>() == null)
        // {
        if (!serverLogged)
        {
            if (highscoreServer == null)
            {
                highscoreServer = gameObject.AddComponent<Server>();
            }
            highscoreServer.POST(playerScore, username);
            serverLogged = true;
        }
    }

    private void enableGameOverMenu() {

        gameOverMenu.SetActive(true);
        vivePointers.SetActive(true);
    }

    private void disableGameOverMenu() {

        gameOverMenu.SetActive(false);
        vivePointers.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if (healthBoxes.Count == 0) {

            scoreLabel.text = Score.ToString();
            setWavesActive(false);
            logScore(Score);
            enableGameOverMenu();
        }
        else {

            foreach (GameObject box in GameObject.FindGameObjectsWithTag("HealthBox")) {    // If box withn player area, add it to lists so drones can target it.
                if (!healthBoxes.Contains(box) && Vector3.Distance(mapCenter.position, box.transform.position) < PlayerBoxOwnershipRadius) {
                    healthBoxes.Add(box);
                    availableBoxes.Add(box);
                    // Change respawn location so it respawns a
                    box.GetComponent<Respawn>().RespawnLocation = mapCenter.position + BoxRespawnOffset;
                }
            }

            foreach (GameObject box in healthBoxes) {       // Loop through the relevant boxes, and see if any of them are too far away.

                if ((box.transform.position - mapCenter.position).magnitude > DistanceToLooseBox) {
                    healthBoxes.Remove(box);
                    availableBoxes.Remove(box);
                    Destroy(box);
                }
            }   
        }
		
	}
}
