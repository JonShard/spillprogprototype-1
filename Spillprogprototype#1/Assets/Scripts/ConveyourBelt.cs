﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyourBelt : MonoBehaviour {

    public int MaxItemsOnBelt = 15;
    public float SpawnitemCooldown = 5;     // The time in seconds between each item spawn.
    public List<GameObject> SpawnList;      // A list of prefabs this belt will spawn.


    bool spawningEnabled;
    List<GameObject> SpawnedItems;          // Contains references to all items the bels has spawned, so it can later them later.
    float timeLeft;
    Transform container;
    Transform nodes;


	// Use this for initialization
	void Start () {
        spawningEnabled = false;
        timeLeft = SpawnitemCooldown;
        container = transform.Find("ItemsContainer");
        nodes = transform.Find("ConveyourNodes");
        
        if (SpawnList.Count == 0) {
            Debug.LogWarning("Convayourbelt's spawnList can not be empty.");
        }
        if (nodes.childCount <= 1) {
            Debug.LogWarning("Convayourbelt's ConveyourNodes object must have at least 2 nodes.");
        }
        SpawnedItems = new List<GameObject>();
	}

    public void SetSpawningEnabled(bool state) {
        spawningEnabled = state;
        if (!spawningEnabled) {
            nodes.GetChild(nodes.transform.childCount - 1).GetComponent<ConveyourNode>().AbsorbingNode = true;
        }
        else {
            nodes.GetChild(nodes.transform.childCount - 1).GetComponent<ConveyourNode>().AbsorbingNode = false;
        }
    }


    // Update is called once per frame
    void Update () {
        if (spawningEnabled) {
            if (timeLeft > 0) {
                timeLeft -= Time.deltaTime;
            }
            else {
                timeLeft = SpawnitemCooldown;
                if (container.childCount < MaxItemsOnBelt) {
                    // Spawn item.
                    GameObject clone = Instantiate(SpawnList[Random.Range(0, SpawnList.Count-1)], nodes.GetChild(0).position, new Quaternion(), container);
                
                    // Setup patrol script in item.
                    Patrol patrol = clone.AddComponent<Patrol>();
                    for (int i = 0; i < nodes.childCount; i++) {
                        ConveyourNode node = nodes.GetChild(i).gameObject.GetComponent<ConveyourNode>();
                        patrol.AddNode(node);
                    }

                }
            }
        }

    }

}
