﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcePullable : MonoBehaviour {

    public float MaxPullSpeed = 10;
    public bool Pullable = true;
    public bool Pushabe = true;

    Rigidbody rig;
    Vector3 angleVel;
    bool isPulled = false;

    public bool isBeingPulled() {

        return isPulled;
    }


	// Use this for initialization
	void Start () {
        angleVel = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

        rig = this.gameObject.GetComponent<Rigidbody>();        // Find the pullable object's rigidbody:
        if (rig == null) {

            Debug.LogWarning("Missing component, checking parent.");
            rig = this.GetComponentInParent<Rigidbody>();

            if (rig != null) {

                Debug.Log("Using Rigidbody body in parent.");
            }
        }
        else {
            Debug.Log("Found rig firsat try");
        }
        if (rig == null) {
            Debug.LogError("Missing component!! No rigidbodyin parent.");
        }
    }


    private void OnTriggerEnter(Collider other) {
        if (rig != null && isPulled) {

            rig.useGravity = false;
        }
    }

    private void OnTriggerStay(Collider other) {                                            // While inside trigger / collider.

        //Debug.Log("Stay: " + other.name + "\t parent: " + other.transform.parent.name);

        if (rig != null             // If has rigidbody and the trigger is a part of a ForcePull object like a gravitygun. Also if im a healthBox, i wont be affcted if mode is AntiGavity.
            && (other.transform.name == "PullSphere" || other.transform.name == "PullBeam") 
            &&  other.transform.parent.parent.parent.GetComponent<ForcePull>().isOn) {

            if (    (other.transform.parent.parent.parent.GetComponent<ForcePull>().ActiveConfig.ForceMultiplier > 0 && Pullable) 
                ||  (other.transform.parent.parent.parent.GetComponent<ForcePull>().ActiveConfig.ForceMultiplier < 0 && Pushabe)) {

                Transform center = other.transform.parent.Find("Center").transform;             // Transform of the center gameObject which is the center of the stasis bubble.
                Vector3 distance = center.transform.position - gameObject.transform.position;
                ForcePull pull = other.transform.parent.parent.parent.GetComponent<ForcePull>();

                if (distance.magnitude > pull.ActiveConfig.StasisRadius) {                                   // If still within beam range. Not in stasis yet.
                    rig.AddForce(distance.normalized * pull.ActiveConfig.ForceMultiplier, ForceMode.Acceleration);    // Pull objects in. ("beam")
                    rig.velocity = Vector3.ClampMagnitude(rig.velocity, MaxPullSpeed);
                }
                else {
                    rig.angularVelocity = new Vector3(angleVel.x, angleVel.y * pull.ActiveConfig.StasisAngleVelocity, angleVel.z * pull.ActiveConfig.StasisAngleVelocity); // Spinn onjects to make them easier to grab.
                    rig.velocity = Vector3.ClampMagnitude((distance).normalized, pull.ActiveConfig.StasisForce * ((distance.magnitude - pull.ActiveConfig.StasisStopDistance) / pull.ActiveConfig.StasisRadius));
                }
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (rig != null) {

            rig.useGravity = true;
            isPulled = false;
        }
    }
}
