﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreDisplay : MonoBehaviour {

    Text scoreLabel;
    GameManager gameManager;
    // Use this for initialization
    void Start () {
		scoreLabel = transform.Find("Canvas").transform.Find("lbl_scorevalue").GetComponent<Text>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>(); // GameObject.Find is expensive. Must only be done on startup.
        if (gameManager == null) {
            Debug.LogError("There are no GameManager in the scene with the name 'GameManager'");
        }
    }

    // Update is called once per frame
    void Update () {
        scoreLabel.text = gameManager.GetScore().ToString();
	}
}
