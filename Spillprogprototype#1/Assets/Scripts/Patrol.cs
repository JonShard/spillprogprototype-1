﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {

    public float PatrolSpeed = 2;
    public float NodeSwitchDistance = 0.2f;
    public List<ConveyourNode> Nodes;

    private Rigidbody rig;
    int currentNode = 0;

	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody>();
        if (Nodes == null) {
            Nodes = new List<ConveyourNode>();
        }
	}
	
    public int AddNode(ConveyourNode node) {
        if (Nodes == null) {
            Debug.LogWarning("List not instantiated. Making it now.");
            Nodes = new List<ConveyourNode>();
        }
        Nodes.Add(node);
        return Nodes.Count;
    }

	// Update is called once per frame
	void FixedUpdate () {
        Vector3 distance = Nodes[currentNode].transform.position - transform.position;

        if (Nodes[currentNode].AbsorbingNode && distance.magnitude < NodeSwitchDistance) {
            Destroy(gameObject);
        }

        if (Nodes[currentNode].Teleport) {
            transform.position = Nodes[currentNode].transform.position;
        }
        else {
            rig.velocity = (distance.normalized * PatrolSpeed);
        }

        if (distance.magnitude < NodeSwitchDistance) {
            currentNode++;
            currentNode %= Nodes.Count;
        }
	}

    private void OnTriggerEnter(Collider other) {
        if (other.name == "PullSphere" || other.name == "PullBeam") {
            this.enabled = false;
        }
    }
}
