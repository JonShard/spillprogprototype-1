﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour {


    public float StartDelay = 3;                // Time from the wave is activated til the first enemy spawns.
    public float InterEnemySpawnDelay = 0;      // Time from one enemy spawns, to the next one spawn.
    public int EarlyDeactivation = 0;           // The number of enemies that are allowed to remain before the wave is deactivated, 
                                                // causing the next wave to start before all the enemies of this one is dead.
    public List<GameObject> Enemies;

    float spawnDelay;
    int enemiesSpawned;

    // Use this for initialization
    void Start () {
        spawnDelay = InterEnemySpawnDelay;

        for (int i = 0; i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(false);      // Deactivates all children incase they are not already.
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (StartDelay - spawnDelay <= 0) {      // Delayed start? Wait intil delay has become less than 0.

            if (spawnDelay <= 0) {

                spawnDelay = InterEnemySpawnDelay;
                if (enemiesSpawned < transform.childCount) {
                    transform.GetChild(enemiesSpawned++).gameObject.SetActive(true);   // When wave activates, enable all the children of the wave.
                }
            }
            else {

                spawnDelay -= Time.deltaTime;
            }
        }
        else {

            StartDelay -= Time.deltaTime;
        }
    
        if (transform.childCount <= EarlyDeactivation) {        // If its time to detach children an destroy wave, in order for the next wave to start.

            // TODO: Set my children to as children of next wave instead of to root.
            transform.DetachChildren();
            Destroy(gameObject);
        }

	}
}

