﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckEngine : MonoBehaviour {

    private void OnDestroy() {
                       
        // If have Configurable Joint and connected thing has DroneBehaviour.
        if (GetComponent<ConfigurableJoint>() && 
            GetComponent<ConfigurableJoint>().connectedBody && 
            GetComponent<ConfigurableJoint>().connectedBody.gameObject.GetComponent<DroneBehaviour>()) {

                        // Greatly reduce its hoverforce so it flies less better :P.
            GetComponent<ConfigurableJoint>().connectedBody.gameObject.GetComponent<DroneBehaviour>().hoverForce /= 4;
        }
    }
}
