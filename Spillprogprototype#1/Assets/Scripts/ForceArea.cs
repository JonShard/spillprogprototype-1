﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ForceArea : MonoBehaviour {
    [Range(0, 100)]
    public float Force = 20;

    private void OnTriggerStay(Collider other) {
        if(other.GetComponent<Rigidbody>()) {
            other.GetComponent<Rigidbody>().AddForce(-transform.forward * Force, ForceMode.Acceleration);
        }
    }
}
