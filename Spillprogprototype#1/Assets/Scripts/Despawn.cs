﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawn : MonoBehaviour {

    public float DespawnHeight = -10;

	// Update is called once per frame
	void FixedUpdate () {
		if (transform.position.y < DespawnHeight) {
            Destroy(gameObject);
        }
	}
}
