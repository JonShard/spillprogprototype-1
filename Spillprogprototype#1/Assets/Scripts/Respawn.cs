﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Respawn : MonoBehaviour {

    public Vector3 RespawnLocation;
    public bool RespawnIfBelowHeight = true;
    public float RespawnHeight = -10;

    public void RespawnObject() {
        transform.position = RespawnLocation;
        transform.rotation = new Quaternion(0, 0, 0, 0);

        if (GetComponent<Rigidbody>() != null) {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    } 

	// Use this for initialization
	void Start () {
        if (RespawnLocation == Vector3.zero) {                      // Save initial position to tp back to.

            RespawnLocation = transform.position;
        }
	}
	

	// Update is called once per frame
	void FixedUpdate () {
		if (RespawnIfBelowHeight && transform.position.y < RespawnHeight) {          // If too low respawn.
            RespawnObject();
        }
	}
}
