﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParentOnDeath : MonoBehaviour {

    GameObject parent;

    private void Start() {
        parent = transform.parent.gameObject;   // Must be stored since the object has been taken out of the hierarchy by the time OnDestroy is called.
                                                // It's kinda dumb.
    }

    private void  OnDestroy () {
        Destroy(parent);
    }
}
