﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhysicalButton : MonoBehaviour {

    public float buttonTravelDistance = 0.2f;
    Transform buttonHead;
    Vector3 buttonStartPos;
    Text countdown;

    bool countdownFinished = false;
    float timeLeft = 0;
    string readyText = "Lower\nelevator";

    // Use this for initialization
    void Start () {
        buttonHead = transform.Find("ButtonHead").transform;
        buttonStartPos = buttonHead.position;
        countdown = GetComponentInChildren<Text>();
        countdown.text = readyText;
    }

    private void Update() {
        
        if (timeLeft > 0) {
            timeLeft -= Time.deltaTime;
            countdown.text = "" + (int)timeLeft;
        } 
        else {
            countdownFinished = true;
            countdown.text = readyText;
        }
    }

    public void SetCountdownTime(int seconds) {
        countdownFinished = false;
        timeLeft = seconds;
    }

    private void OnTriggerEnter(Collider other) {
        if (countdownFinished) {
            buttonHead.position = buttonStartPos + new Vector3(0, -buttonTravelDistance, 0);
            GameObject.Find("Elevator").GetComponent<Elevator>().CallElevator();
        }
        else {
            SetCountdownTime(5);
        }
    }

    private void OnTriggerExit(Collider other) {
        buttonHead.position = buttonStartPos;
    }

}
