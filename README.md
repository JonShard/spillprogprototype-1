# Game programming prototype #1
This is the first protytpe in the course IMT3601 at NTNU in Gj�vik, Norway.
The game we are making is a VR game in Unity based around throwing knives on flying drones.

### Participants:
| Name                  | Student no |
| --------------------- | ---------- |
| Herman Dybing         | 473207     | 
| Jone Skaara 	        | 473181     |
| Askel Eirik Johansson | 473215     |